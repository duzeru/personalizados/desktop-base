# desktop-base

Este pacote contém vários arquivos genéricos que são usados pelas instalações "Desktop" do Debian. Atualmente, fornece algum trabalho artístico e temas associados ao Debian, arquivos .desktop contendo links para material associado ao Debian (adequado para ser posto no Desktop do usuário), e outros arquivos comuns aos ambientes de desktops disponíveis como o GNOME e o KDE.